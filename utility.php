<?php

if( !class_exists( 'shrewd_option' ) )
{
	class shrewd_option
	{
		/**
		 * Sets a Wordpress option
		 * @param string $name The name of the option to set
		 * @param string $value The value of the option to set
		 */
		public static function set( $name, $value )
		{
			if (get_option($name) !== FALSE)
			{
				update_option($name, $value);
			}
			else
			{
				$deprecated = '';
				$autoload   = 'no';
				add_option($name, $value, $deprecated, $autoload);
			}
		}

		/**
		 * Gets a Wordpress option
		 * @param string    $name The name of the option
		 * @param mixed     $default The default value to return if one doesn't exist
		 * @return string   The value if the option does exist
		 */
		public static function get( $name, $default = FALSE )
		{
			$value = get_option($name);
			if( $value !== FALSE ) return stripslashes ($value);
			return $default;
		}
	}
}

if( !class_exists( 'shrewd_views' ) )
{
	class shrewd_views
	{
		/**
		 * Load a view file. The file should be located in WPSearch/Views.
		 * @param string $file The filename of the view without the extenstion (assumed
		 *  to be PHP)
		 * @param array $data An associative array of data that be be extracted and
		 *  available to the view
		 */
		public static function load( $file, $data = array(), $returnString = FALSE )
		{
			$file = dirname(__FILE__) . '/views/' . $file . '.php';

			if(!file_exists($file))
			{
				//WPSearch_Log::add('fatal', "View '$file' was not found");
				throw new Exception("View '$file' was not found");
			}

			# Extract the variables into the global scope so the views can use them
			extract($data);

			if( $returnString )
			{
				ob_start( );

				try
				{
					// Load the view within the current scope
					include( $file );
				}
				catch( Exception $e )
				{
					// Delete the output buffer
					ob_end_clean( );

					// Re-throw the exception
					throw $e;
				}

				// Get the captured output and close the buffer
				return ob_get_clean( );
			}
			else
			{
				include($file);
			}
		}
	}
}

if( !class_exists( 'shrewd_array' ) )
{
	class shrewd_array 
	{
		/**
		 * Get a value from an associative array. The specified key may or may
		 *  not exist.
		 * @param array $array Array to grab the value from
		 * @param mixed $key The key to check the array
		 * @param mixed $default A value to return if the key doesn't exist int he array (default is FALSE)
		 * @return mixed The value if the key exists, and the default if it doesn't
		 */
		public static function get($array, $key, $default = FALSE)
		{
			if(array_key_exists($key, $array))
				return $array[$key];
			else
				return $default;
		}
	}
}
