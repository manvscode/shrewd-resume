<style type="text/css">
	ul.circle { margin-left: 4em; list-style-type:circle}
	td {
		padding: 5px 0px 5px 8px;
	}
	tr th[scope=row], tr td {
		vertical-align: top;
	}
	.shrewd_resume_header, .shrewd_resume_footer {
		width: 600px;
		height: 100px;
	}
	.shrewd_resume_body {
		min-width: 600px;
		max-width: 100%;
		height: 350px;
	}

    #my_info {
        padding: 10px;
        background-color: #fcfcfc;
        margin-right: 10px;
        float:right;
        width: 250px;
        border: 1px solid #c0c0c0;
		border-radius: 10px;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		box-shadow: 2px 2px 5px #333 !important;
		-moz-box-shadow: 2px 2px 5px #333 !important;
		-webkit-box-shadow: 2px 2px 5px #333 !important;
    }

    #my_info .name {
        color: #ffbb00;
        font-weight: bold;
    }
    
    p.message {
         background: #FFFBD8;
         border: 1px solid #F5EEAD;
         color: #333;
         margin: 10px 5px 5px 5px;
         padding: 10px;
    }
</style>
<h2> Your Resume </h2>

<?php if($is_updated): ?>
    <p class="message">
        Your resume have been updated.
    </p>
<?php endif; ?>
<div id="my_info">
    Written by <span class="name"> Joe Marrero </span> 
    Find him at <a href="http://www.manvscode.com/">manvscode.com</a>. Reach him via <a href="mailto:manvscode@gmail.com">manvscode@gmail.com</a> with any questions!
</div>

<p> You can use the following variables in your wordpress content: </p>
<pre>
      <strong>{{RESUME_NAME}}</strong>             Your Name.
      <strong>{{RESUME_HEADER}}</strong>           Your Resume's Header.
      <strong>{{RESUME_BODY}}</strong>             Your Resume's Body.
      <strong>{{RESUME_FOOTER}}</strong>           Your Resume's Footer.
      <strong>{{RESUME_WEB_URL}}</strong>          The URL for the generated web version.
      <strong>{{RESUME_PDF_URL}}</strong>          The URL for the generated PDF version.
      <strong>{{RESUME_DOC_URL}}</strong>          The URL for the generated Microsoft Word version.
      <strong>{{RESUME_TXT_URL}}</strong>          The URL for the generated plain textfile version.
</pre>
<p> Your generated resumes can be accessed from the following URLs: </p>
<?php 
$url = home_url();
?>
<ul class="circle">
	<li> <a href="<?php echo "$url/resume/web"; ?>"> Web Version </a> </li>
	<li> <a href="<?php echo "$url/resume/pdf"; ?>"> PDF Version </a> </li>
	<li> <a href="<?php echo "$url/resume/doc"; ?>"> Microsoft Word Version </a> </li>
	<li> <a href="<?php echo "$url/resume/txt"; ?>"> Plain Textfile Version </a> </li>
</ul>

<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
    <table id="shrewd_settings">
        <tr>
            <th scope="row">
                Your Name
            </th>
            <td>
                <input class="" type="text" name="resume_name" value="<?php echo $resume_name; ?>" />
            </td>
        </tr>
        <tr>
            <th scope="row">
                Header
            </th>
            <td>
                <textarea wrap="off" class="shrewd_resume_header" name="resume_header"><?php echo $resume_header; ?></textarea>
            </td>
        </tr>
        <tr>
            <th scope="row">
                Body 
            </th>
            <td>
                <textarea wrap="off" class="shrewd_resume_body" name="resume_body"><?php echo $resume_body; ?></textarea>
            </td>
        </tr>
        <tr>
            <th scope="row">
                Footer
            </th>
            <td>
                <textarea wrap="off" class="shrewd_resume_footer" name="resume_footer"><?php echo $resume_footer; ?></textarea>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <input name="submit_btn" type="submit" value="Save" />
            </th>
            <td>
            </td>
        </tr>
    </table>
</form>
<?php
