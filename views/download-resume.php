<?php if( is_page( $display_on_page ) ): ?>
	<?php
	echo $before_widget;
	$txtEnabled = shrewd_option::get( 'shrewd-resume-enable-txt', FALSE );
	$docEnabled = shrewd_option::get( 'shrewd-resume-enable-doc', FALSE );
	$pdfEnabled = shrewd_option::get( 'shrewd-resume-enable-pdf', FALSE );
	?>

	<h1> <?php echo $before_title . $title . $after_title; ?> </h1>
	<ul>
	<li> <a title="Download Web Version" target="_blank" href="<?php echo home_url().'/resume/web'; ?>">Web Version</a> </li>
	<?php if( $pdfEnabled ): ?>
	<li> <a title="Download PDF" target="_blank" href="<?php echo home_url().'/resume/pdf'; ?>">PDF Version</a> </li>
	<?php endif; ?>
	<?php if( $docEnabled ): ?>
	<li> <a title="Download Microsoft Word Document" target="_blank" href="<?php echo home_url().'/resume/doc'; ?>">DOC Version</a> </li>
	<?php endif; ?>
	<?php if( $txtEnabled ): ?>
	<li> <a title="Download Plain Textfile" target="_blank" href="<?php echo home_url().'/resume/txt'; ?>">Text Version</a> </li>
	<?php endif; ?>
	</ul>
	<?php echo $after_widget; ?>
<?php endif; ?>
