<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="The resume of <?php echo $resume_name; ?>"/>
	<meta name="author" content="<?php echo $resume_name; ?>" />
	<link rel="stylesheet" type="text/css" href="" media="screen,print" />
	<title> The resume of <?php echo $resume_name; ?> </title>
	<style type="text/css" media="screen,print">
		<?php echo $resume_css; ?>
	</style>
</head>
<body>
	<?php echo $resume_header; ?>
	<?php echo $resume_body; ?>
	<?php echo $resume_footer; ?>
</body>
</html>
