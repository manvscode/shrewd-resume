<style type="text/css">

	td {
		padding: 5px 0px 5px 8px;
	}
	tr th[scope=row], tr td {
		vertical-align: top;
	}
	.shrewd_textarea {
		width: 600px;
		height: 700px;
	}

    #my_info {
        padding: 10px;
        background-color: #fcfcfc;
        margin-right: 10px;
        float:right;
        width: 250px;
        border: 1px solid #c0c0c0;
		border-radius: 10px;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		box-shadow: 2px 2px 5px #333 !important;
		-moz-box-shadow: 2px 2px 5px #333 !important;
		-webkit-box-shadow: 2px 2px 5px #333 !important;
    }

    #my_info .name {
        color: #ffbb00;
        font-weight: bold;
    }
    
    p.message {
         background: #FFFBD8;
         border: 1px solid #F5EEAD;
         color: #333;
         margin: 10px 5px 5px 5px;
         padding: 10px;
    }
</style>
<h2> Resume Settings </h2>

<?php if($is_updated): ?>
    <p class="message">
        Your settings have been updated.
    </p>
<?php endif; ?>

<div id="my_info">
    Written by <span class="name"> Joe Marrero </span> 
    Find him at <a href="http://www.manvscode.com/">manvscode.com</a>. Reach him via <a href="mailto:manvscode@gmail.com">manvscode@gmail.com</a> with any questions!
</div>

<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
    <table id="shrewd_settings">
        <tr>
            <th scope="row">
				Show Header? &nbsp;
            </th>
            <td>
                <input name="enable_header" type="checkbox" value="1" <?php echo ($enable_header ? "checked='checked'" : ''); ?> />
            </td>
        </tr>
        <tr>
            <th scope="row">
				Show Footer? &nbsp;
            </th>
            <td>
                <input name="enable_footer" type="checkbox" value="1" <?php echo ($enable_footer ? "checked='checked'" : ''); ?> />
            </td>
        </tr>
        <tr>
            <th scope="row">
				PDF Download? &nbsp;
            </th>
            <td>
                <input name="enable_pdf" type="checkbox" value="1" <?php echo ($enable_pdf ? "checked='checked'" : ''); ?> />
            </td>
        </tr>
        <tr>
            <th scope="row">
				Word Download? &nbsp;
            </th>
            <td>
                <input name="enable_doc" type="checkbox" value="1" <?php echo ($enable_doc ? "checked='checked'" : ''); ?> />
            </td>
        </tr>
        <tr>
            <th scope="row">
                Resume CSS
            </th>
            <td>
                <textarea wrap="off" class="shrewd_textarea" name="resume_css"><?php echo $resume_css; ?></textarea>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <input name="submit_btn" type="submit" value="Save" />
            </th>
            <td>
            </td>
        </tr>
    </table>
</form>
<?php
