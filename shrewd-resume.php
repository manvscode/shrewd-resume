<?php
/*
	Plugin Name: Shrewd Resume
	Plugin URI: http://www.manvscode.com/
	Description: Manage your resume from Wordpress and generate versions in PDF and Microsoft Word.
	Version: 0.1
	Author: Joe Marrero
	Author URI: http://www.manvscode.com
	License: GPL2

	Copyright 2012  Joe Marrero  (email: manvscode@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
require_once dirname(__FILE__) . '/utility.php';

define("DOMPDF_TEMP_DIR", DOCROOT.'/tmp');
//define("DOMPDF_CHROOT", DOMPDF_DIR);
//define("DOMPDF_UNICODE_ENABLED", false);
//define("TTF2AFM", "C:/Program Files (x86)/GnuWin32/bin/ttf2pt1.exe");
define( 'DOMPDF_PDF_BACKEND', 'PDFLib' );
define( 'DOMPDF_DEFAULT_MEDIA_TYPE', "screen");
define( 'DOMPDF_DEFAULT_PAPER_SIZE', "letter");
define( 'DOMPDF_DEFAULT_FONT', "Verdana");
define( 'DOMPDF_DPI', 95 );
define( 'DOMPDF_ENABLE_PHP', TRUE);
define( 'DOMPDF_ENABLE_JAVASCRIPT', TRUE);
define( 'DOMPDF_ENABLE_REMOTE', TRUE );
//define("DEBUGPNG", true);
//define("DEBUGKEEPTEMP", true);
//define("DEBUGCSS", true);
//define("DEBUG_LAYOUT", true);
//define("DEBUG_LAYOUT_LINES", false);
//define("DEBUG_LAYOUT_BLOCKS", false);
//define("DEBUG_LAYOUT_INLINE", false);
//define("DEBUG_LAYOUT_PADDINGBOX", false);


// Load DOMPDF configuration, this will prepare DOMPDF
require_once dirname(__FILE__) . '/lib/dompdf-latest/dompdf_config.inc.php';


add_action( 'init', array('ShrewdResume', 'createInternalUrl') );
add_filter( 'query_vars', array('ShrewdResume', 'insertQueryVariables') );
add_action( 'parse_request', array('ShrewdResume', 'handleRequests') );
add_action( 'admin_menu', array('ShrewdResumeOptions', 'registerAdmin') );
add_filter( 'the_content',  array('ShrewdResume', 'replaceVariables'));
add_action( 'widgets_init', array('ShrewdResume', 'registerWidgets') );

class ShrewdDownloadResume_Widget extends WP_Widget
{
	public function __construct()
	{
		parent::__construct(
	 		'shrewddownloadresume_widget', // Base ID
			'Shrewd Download Resume', // Name
			array( 'description' => __( 'Download Resume Widget', 'text_domain' ), ) // Args
		);
	}

 	public function form( $instance )
	{
		if( isset( $instance[ 'title' ] ) )
		{
			$title = $instance[ 'title' ];
		}
		else
		{
			$title = __( 'Download Resume', 'text_domain' );
		}
		
		if( isset( $instance[ 'display_on_page' ] ) )
		{
			$display_on_page = $instance[ 'display_on_page' ];
		}
		else
		{
			$display_on_page = 'home';
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
		<label for="<?php echo $this->get_field_id( 'display_on_page' ); ?>"><?php _e( 'Display Only On:' ); ?></label> 
		<?php $pages = get_pages( array(
				'sort_order' => 'ASC',
				'sort_column' => 'post_title',
				'hierarchical' => 1,
				'post_type' => 'page',
				'post_status' => 'publish') ); ?> 

		<select class="widefat" id="<?php echo $this->get_field_id( 'display_on_page' ); ?>" name="<?php echo $this->get_field_name( 'display_on_page' ); ?>">
			<?php foreach( $pages as $page ): ?>
				<?php if( $page->post_name == $display_on_page ): ?>
				<option value="<?php echo $page->post_name; ?>" selected="selected"> <?php echo $page->post_title; ?> </option>
				<?php else: ?>
				<option value="<?php echo $page->post_name; ?>"> <?php echo $page->post_title; ?> </option>
				<?php endif; ?>
			<?php endforeach; ?>
		</select>
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance )
	{
		$instance = array();
		$instance['title']           = strip_tags( $new_instance['title'] );
		$instance['display_on_page'] = strip_tags( $new_instance['display_on_page'] );

		return $instance;
	}

	public function widget( $args, $instance )
	{
		//extract( $args );

		$data = $args;

		$data[ 'title' ]          = apply_filters( 'widget_title', $instance['title'] );
		$data[ 'display_on_page'] = $instance['display_on_page'];

		shrewd_views::load( 'download-resume', $data );
	}
}


class ShrewdResume 
{
	static function createInternalUrl()
	{
        $txtEnabled = shrewd_option::get( 'shrewd-resume-enable-txt', FALSE );
    	$docEnabled = shrewd_option::get( 'shrewd-resume-enable-doc', FALSE );
        $pdfEnabled = shrewd_option::get( 'shrewd-resume-enable-pdf', FALSE );

		$typeRegEx = 'web';

		if( $txtEnabled )  $typeRegEx .= '|txt';
		if( $docEnabled )  $typeRegEx .= '|doc';
		if( $pdfEnabled )  $typeRegEx .= '|pdf';

		//add_rewrite_rule( 'resume/?$', 'index.php?shrewd_resume=1', 'top' );
		add_rewrite_rule( "resume/($typeRegEx)/?$", 'index.php?shrewd_resume=1&resume_type=$matches[1]', 'top' );

		//exit( shrewd_option::get( 'shrewd-resume-body' ) );
	}

	static function registerWidgets( )
	{
    	return register_widget( 'ShrewdDownloadResume_Widget' );
	}

	static function insertQueryVariables( $query_vars )
	{
		$query_vars[] = 'shrewd_resume';
		$query_vars[] = 'resume_type';
		return $query_vars;
	}

	function handleRequests( &$wp )
	{
		if( array_key_exists( 'shrewd_resume', $wp->query_vars ) )
		{
			$type = shrewd_array::get( $wp->query_vars, 'resume_type', 'web' );

			$data = array(
				'resume_css'    => shrewd_option::get( 'shrewd-resume-css', '' ),
				'resume_name'   => shrewd_option::get( 'shrewd-resume-name', '' ),
				'resume_author' => shrewd_option::get( 'shrewd-resume-author', '' ),
				'resume_header' => shrewd_option::get( 'shrewd-resume-header', '' ),
				'resume_body'   => shrewd_option::get( 'shrewd-resume-body', '' ),
				'resume_footer' => shrewd_option::get( 'shrewd-resume-footer', '' ),
			);

			foreach( $data as $key => &$value )
			{
				$value = ShrewdResume::replaceVariables( $value );
			}

			switch( $type )
			{
				case 'pdf':
					header( 'Content-Type: application/pdf' );
					// Render the HTML normally
        			$html = shrewd_views::load( 'resume', $data, TRUE );

					// Turn off strict errors, DOMPDF is stupid like that
					$ER = error_reporting(~E_STRICT);

					// Render the HTML to a PDF
					$pdf = new DOMPDF;
					$pdf->load_html($html);
					$pdf->render();

					// Restore error reporting settings
					error_reporting($ER);

					echo $pdf->output();
					break;
				case 'txt':
					echo "Not implemented.";
					break;
				case 'doc':
					echo "Not implemented.";
					break;
				case 'web': // fall-through
				default:
        			shrewd_views::load( 'resume', $data );
					break;
			}
					
			exit();
		}
	}


    /**
     * A callback that will parse out things in the form {{source:username}} and
     *  replace it with the project list. Ex, {{github:katzgrau}}
     * @param string $content
     * @return string The updated content
     */
	static function replaceVariables( $content )
	{
		$header = '';
		$footer = '';

		if( shrewd_option::get('shrewd-resume-enable-header', FALSE) == TRUE )
		{
			$header = shrewd_option::get('shrewd-resume-header', '');
		}

		if( shrewd_option::get('shrewd-resume-enable-footer', FALSE) == TRUE )
		{
			$footer = shrewd_option::get('shrewd-resume-footer', '');
		}

		$content = str_replace( '{{RESUME_NAME}}', shrewd_option::get('shrewd-resume-name', ''), $content );
		$content = str_replace( '{{RESUME_HEADER}}', $header, $content );
		$content = str_replace( '{{RESUME_BODY}}', shrewd_option::get('shrewd-resume-body', ''), $content );
		$content = str_replace( '{{RESUME_FOOTER}}', $footer, $content );
		$content = str_replace( '{{RESUME_WEB_URL}}', home_url().'/resume/web', $content );
		$content = str_replace( '{{RESUME_PDF_URL}}', home_url().'/resume/pdf', $content );
		$content = str_replace( '{{RESUME_DOC_URL}}', home_url().'/resume/doc', $content );
		$content = str_replace( '{{RESUME_TXT_URL}}', home_url().'/resume/txt', $content );
		return $content;
	}
}



class ShrewdResumeOptions
{
	/**
	 * Register the admin settings page
	 */
	static function registerAdmin( )
	{
		$icon = plugins_url( 'shrewd-resume/images/resume.png' );
		add_menu_page( 'Shrewd Resume', 'Resume', 'edit_pages', 'shrewd-resume', array(__CLASS__, 'editResume'), $icon );
		add_submenu_page( 'shrewd-resume', 'Settings', 'Settings', 'edit_pages', 'shrewd-resume-settings', array(__CLASS__, 'editSettings') );
	}

    /**
     * The function used by WP to print the admin settings page
     */
    static function editResume()
    {
        $submit  = shrewd_array::get($_POST, 'submit_btn');
        $updated = FALSE;

        if($submit)
        {
            shrewd_option::set( 'shrewd-resume-name', shrewd_array::get($_POST, 'resume_name'));
            shrewd_option::set( 'shrewd-resume-header', shrewd_array::get($_POST, 'resume_header'));
            shrewd_option::set( 'shrewd-resume-body', shrewd_array::get($_POST, 'resume_body'));
            shrewd_option::set( 'shrewd-resume-footer', shrewd_array::get($_POST, 'resume_footer'));

            $updated = TRUE;
        }

        $data = array (
            'resume_name'   => shrewd_option::get( 'shrewd-resume-name', 'John Doe' ),
            'resume_header' => shrewd_option::get( 'shrewd-resume-header', '' ),
            'resume_body'   => shrewd_option::get( 'shrewd-resume-body', '' ),
            'resume_footer' => shrewd_option::get( 'shrewd-resume-footer', '' ),
            'is_updated'    => $updated
        );

        shrewd_views::load( 'edit-resume', $data );
    }

    static function editSettings()
    {
        $submit  = shrewd_array::get($_POST, 'submit_btn', FALSE);
        $updated = FALSE;

        if($submit)
        {
            shrewd_option::set( 'shrewd-resume-enable-header', shrewd_array::get($_POST, 'enable_header', 0) );
            shrewd_option::set( 'shrewd-resume-enable-footer', shrewd_array::get($_POST, 'enable_footer', 0) );
            shrewd_option::set( 'shrewd-resume-enable-pdf', shrewd_array::get($_POST, 'enable_pdf', 0) );
            shrewd_option::set( 'shrewd-resume-enable-doc', shrewd_array::get($_POST, 'enable_doc', 0) );
            shrewd_option::set( 'shrewd-resume-css', shrewd_array::get($_POST, 'resume_css', '') );

            $updated = TRUE;
        }

        $data = array (
            'enable_header' => shrewd_option::get( 'shrewd-resume-enable-header', TRUE ),
            'enable_footer' => shrewd_option::get( 'shrewd-resume-enable-footer', TRUE ),
            'enable_pdf'    => shrewd_option::get( 'shrewd-resume-enable-pdf', TRUE ),
            'enable_doc'    => shrewd_option::get( 'shrewd-resume-enable-doc', TRUE ),
            'resume_css'    => shrewd_option::get( 'shrewd-resume-css', '' ),
            'is_updated'    => $updated
        );

        shrewd_views::load( 'settings', $data );
    }
}

